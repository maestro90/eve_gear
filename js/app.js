var SAAgent = null;
var SASocket = null;
var CHANNELID = 104;
var ProviderAppName = "EveProvider";

/*
* 순서는 connect -> fetch
* 지금은 onload에서 connect와 fetch를 호출하도록 해놓은 상태.
* fetch를 해야 안드로이드에서 보낸 data가 넘어온다.
*/

function createHTML(log_string) {
	var log = document.getElementById('response');
	log.innerHTML = log.innerHTML + "<br> : " + log_string;
}

function onerror(err) {
	console.log("err [" + err.name + "] msg[" + err.message + "]");
}

var agentCallback = {
	onconnect : function(socket) {
		SASocket = socket;
		// alert("HelloAccessory Connection established with RemotePeer");
		SASocket.setSocketStatusListener(function(reason) {
			console.log("Service connection lost, Reason : [" + reason + "]");
			disconnect();
		});
	},
	onerror : onerror
};

var peerAgentFindCallback = {
	onpeeragentfound : function(peerAgent) {
		try {
			if (peerAgent.appName == ProviderAppName) {
				SAAgent.setServiceConnectionListener(agentCallback);
				SAAgent.requestServiceConnection(peerAgent);
			} else {
				alert("Not expected app!! : " + peerAgent.appName);
			}
		} catch (err) {
			consoleF.log("exception [" + err.name + "] msg[" + err.message + "]");
		}
	},
	onerror : onerror
}

function onsuccess(agents) {
	try {
		if (agents.length > 0) {
			SAAgent = agents[0];

			SAAgent.setPeerAgentFindListener(peerAgentFindCallback);
			SAAgent.findPeerAgents();
			createHTML("connection Success");
		} else {
			alert("Not found SAAgent!!");
		}
	} catch (err) {
		console.log("exception [" + err.name + "] msg[" + err.message + "]");
	}
}

function connect() {
	if (SASocket) {
		alert('Already connected!');
		return false;
	}
	try {
		webapis.sa.requestSAAgent(onsuccess, onerror);
	} catch (err) {
		console.log("exception [" + err.name + "] msg[" + err.message + "]");
	}
}

function disconnect() {
	try {
		if (SASocket != null) {
			SASocket.close();
			SASocket = null;
			createHTML("closeConnection");
			console.log('close');
		}
	} catch (err) {
		console.log("exception [" + err.name + "] msg[" + err.message + "]");
	}
}

//이 함수의 data로 안드로이드에서 보낸 json이 넘어오니까 여기 데이터 받아서 작업 하면돼!!
function onreceive(channelId, data) {
	var result = JSON.parse(data);
	createHTML(data);
}

function fetch() {

	createHTML("fetch");
	try {
		SASocket.setDataReceiveListener(onreceive);
	} catch (err) {
		console.log("exception [" + err.name + "] msg[" + err.message + "]");
	}
}

function string2Bin(str) {
	var result = [];
	for ( var i = 0; i < str.length; i++) {
		result.push(str.charCodeAt(i).toString(2));
	}
	return result;
}

// function bin2String(array) {
// return String.fromCharCode.apply(String, array);
// }
function bin2String(array) {
	var result = "";
	for ( var i = 0; i < array.length; i++) {
		result += String.fromCharCode(parseInt(array[i], 2));
	}
	return result;
}

window.onload = function() {

	connect();
	fetch();
	// add eventListener for tizenhwkey
	document.addEventListener('tizenhwkey', function(e) {
		if (e.keyName == "back")
			tizen.application.getCurrentApplication().exit();
	});
};
